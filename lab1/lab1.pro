#-------------------------------------------------
#
# Project created by QtCreator 2016-10-11T16:30:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab1
TEMPLATE = app


SOURCES += main.cpp\
        texteditor.cpp

HEADERS  += texteditor.h

FORMS    += texteditor.ui
