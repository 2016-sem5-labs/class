#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QProcess>
#include <QTime>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    QProcess m_process;
    QTime m_startTime;
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_browseBtn_clicked();

    void on_startBtn_clicked();

    void on_stopBtn_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
