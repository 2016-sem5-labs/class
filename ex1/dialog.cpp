#include "dialog.h"
#include "ui_dialog.h"
#include <QFileDialog>
#include <QTextCodec>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->startBtn->setEnabled(false);
    ui->stopBtn->setEnabled(false);


    connect(&m_process,&QProcess::stateChanged,[=](QProcess::ProcessState newState){
        switch (newState) {
        case QProcess::Running:
            ui->startBtn->setEnabled(false);
            ui->stopBtn->setEnabled(true);
            break;
        case QProcess::NotRunning:
            ui->startBtn->setEnabled(true);
            ui->stopBtn->setEnabled(false);
            break;
        default:
            break;
        }
    });
    connect(&m_process,&QProcess::readyRead,[=]{
        QTextCodec *codec = QTextCodec::codecForName("IBM 866");
        QString string = codec->toUnicode(m_process.readAllStandardOutput());
        ui->textEdit->insertPlainText(string);
    });

    connect(&m_process,
            static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            [=](int exitCode, QProcess::ExitStatus exitStatus){
        ui->labelExit->setText(tr("Exit code: %1 status: %2").
                               arg(exitCode).arg(exitStatus));
        QTime finishTime = QTime::currentTime();
        ui->labelDuration->setText(QString("Running time: %1 secs").
                                   arg(m_startTime.secsTo(finishTime)));
    });
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_browseBtn_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "",
                                                    tr("Executables (*.exe)"));
    ui->fileName->setText(fileName);
    ui->startBtn->setEnabled(!fileName.isEmpty());
}

void Dialog::on_startBtn_clicked()
{
    if(ui->fileName->text().isEmpty()) return;

    QString program = QString("%1 %2").
            arg(ui->fileName->text()).
            arg(ui->arguments->text());
    m_process.start(program);

    m_process.waitForStarted();
    m_startTime = QTime::currentTime();
    ui->textEdit->clear();

}

void Dialog::on_stopBtn_clicked()
{
    if(m_process.state() == QProcess::Running)
        m_process.terminate();
}
